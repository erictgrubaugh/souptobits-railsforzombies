Book.destroy_all
Genre.destroy_all

fiction = Genre.create!(name: "Fiction")
non_fiction = Genre.create!(name: "Non-Fiction")
software = Genre.create!(name: "Software")

Book.create!([{
  title: "Hyperion",
  author: "Dan Simmons",
  description: "This is a description of Dan Simmons' book Hyperion",
  amazon_id: "8553283685",
  rating: 3,
  finished_on: 10.days.ago,
  genres: [fiction]
},
{
  title: "The Passionate Programmer",
  author: "Chad Fowler",
  description: "A fantastic book about how to be a developer in the business world",
  amazon_id: "1934356344",
  rating: 5,
  finished_on: Date.new(2015,6,15),
  genres: [non_fiction, software]
}])

p "Created #{Genre.count} genres"
p "Created #{Book.count} books"
